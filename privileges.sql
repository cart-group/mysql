USE mysql;

CREATE USER 'product_user'@'localhost' IDENTIFIED BY 'pass';
GRANT ALL ON *.* TO 'product_user'@'localhost';

CREATE USER 'product_user'@'%' IDENTIFIED BY 'pass';
GRANT ALL ON *.* TO 'product_user'@'%';
FLUSH PRIVILEGES;

