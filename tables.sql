CREATE DATABASE IF NOT EXISTS carts;  

USE `carts`;

CREATE TABLE IF NOT EXISTS `cart`(
    `id`          VARCHAR(100) NOT NULL PRIMARY KEY,
    `status`      VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS `order`(
    `id`          VARCHAR(100) NOT NULL PRIMARY KEY,
    `cart_id`          VARCHAR(100) NOT NULL,
    `product_id`       VARCHAR(100) NOT NULL,
    `quantity`      INTEGER NOT NULL,
    FOREIGN KEY (cart_id) REFERENCES cart(id)
);

CREATE DATABASE IF NOT EXISTS products;

